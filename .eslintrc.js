module.exports = {
  "extends": [
    "standard",
    "plugin:vue/recommended"
  ],
  "rules": {
    "vue/html-indent": "error",
    "vue/singleline-html-element-content-newline": "none",
    "vue/no-unused-components": "none",
    "space-before-function-paren": 0,
    "no-undef": 0,
    "arrow-parens": 0,
    "generator-star-spacing": 0,
    "no-console": 2
  },
  "parserOptions": {
    "ecmaVersion": 6
  }
}