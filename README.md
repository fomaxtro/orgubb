# Sistema OrgUBB #

El sistema esta distribuido en una imagen docker 
para su ejecucion. La base de datos fue previamente cargada

### Requisitos ###

- Instalar [Docker](https://docs.docker.com/install/) 
siguiendo los pasos de la pagina oficial
    
###(Opcional) Requisitos para construir imagen Docker ###

- Requisitos minimos [Laravel 5.7](https://laravel.com/docs/5.7/installation)
- git
- composer
- nodejs
- Modulos php
    - php-zip
    - php-mysql

### Instalar Docker image desde Repositorio ###

1. Descargar la imagen desde el registry de Docker
    ```
    docker pull fomaxtro/orgubb:1.0
    ```

2. Ejecutar la imagen
    ```
    docker run -it -p 80:80 fomaxtro/orgubb:1.0
    ```

###(Opcional) Construir imagen Docker ###

1. Clonar el repositorio del proyecto
    ```
   git clone http://gitlab.face.ubiobio.cl:8081/dev-masters/orgubb.git
   ```
   
2. Ingresar a la raiz del proyecto
    ```
   cd orgubb
   ```
   
3. Descargar las dependencias de composer
    ```
   composer install
   ```
   
4. Descargar las dependencias de node
    ```
   npm install
   ```
   
5. Copiar el archivo de variables de entorno de ejemplo
y modificar las secciones indicadas del archivo .env
    ```
   cp .env.example .env
   ```
   - Base de datos
   ```
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=homestead
   DB_USERNAME=homestead
   DB_PASSWORD=secret
   ```
   
   - Relay Email
   ```
   MAIL_DRIVER=smtp
   MAIL_HOST=smtp.mailtrap.io
   MAIL_PORT=2525
   MAIL_USERNAME=null
   MAIL_PASSWORD=null
   MAIL_ENCRYPTION=null
   ```
   
6. Generar la llave de aplicacion
    ```
   php artisan key:generate
   ```
   
7. Compilar el JavaScript
    ```
   npm run prod
   ```
   
8. Generar las llaves de encriptacion de passport
    ```
   php artisan passport:keys
   ```
   
9. Construir la imagen Docker
    ```
   docker build -t orgubb:1.0 .
   ```
   
###(Opcional) Migracion de Base de Datos ###

1. Ingresar a la imagen Docker en modo interactivo
    ```
   docker run -it orgubb:1.0 /bin/bash
   ```
   
2. Ejecutar las migraciones y los seeders
    ```
   php artisan migrate --seed
   ```
   
3. Instalar el cliente passport y las llaves de encriptacion
    ```
   php artisan passport:intall
   ```
   (Observacion): Si ya posee las llaves de encriptacion en la carpeta storage,
   añada el argumento --force al final del comando
