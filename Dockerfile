FROM fomaxtro/laravel:5.7
COPY . /var/www/html
RUN php artisan optimize && php artisan config:clear
RUN chmod -R 777 storage bootstrap/cache
