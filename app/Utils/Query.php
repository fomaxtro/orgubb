<?php


namespace App\Utils;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class Query
{
    /**
     * @param Builder $query
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function search(Builder $query, Request $request)
    {
        if ($request->has(['where',  'value'])) {
            $fields = explode(',', $request->where);

            foreach ($fields as $value) {
                $query->orWhere($value,  'like', "{$request->value}%");
            }
        }

        if ($request->has('perPage')) {
            return $query->paginate((int) $request->perPage);
        }

        return $query->get();
    }
}
