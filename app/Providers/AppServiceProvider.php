<?php

namespace App\Providers;

use Fomaxtro\Roles\Roles;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Roles::ignoreMigrations();
        Roles::routes(['auth:api'], ['index', 'destroy']);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
