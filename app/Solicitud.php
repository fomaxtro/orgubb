<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Solicitud extends Model
{
    protected $fillable = [
        'observation', 'amount', 'start_date', 'end_date', 'type_id', 'status_id', 'association_id', 'user_id'
       ];
   
       public function association(){
           return $this->belongsTo(Association::class);
       }
   
       public function status(){
           return $this->belongsTo(Status::class);
       }
   
       public function type(){
           return $this->belongsTo(Type::class);
       }

       public function file(){
        return $this->belongsTo(File::class);
    }


}
