<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ratificar extends Model
{
    protected $fillable = [
        'observation', 'type_id', 'status_id', 'association_id', 'user_id', 'file_id', 'path'
       ];
   
       public function association(){
           return $this->belongsTo(Association::class);
       }
   
       public function status(){
           return $this->belongsTo(Status::class);
       }
   
       public function type(){
           return $this->belongsTo(Type::class);
       }

      public function user(){
        return $this->belongsTo(User::class);
    }
      
}
