<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.layout')
            ->from(env('MAIL_USERNAME'), 'OrgUBB')
            ->subject('Cambio de Contraseña')
            ->with([
                'title' => 'Cambio de Contraseña',
                'content' => 'Se ha solicitado un cambio de contraseña para el usuario ' . $this->data->email,
                'sub_content' => 'Seleccione el siguiente enlace para continuar',
                'url' => '/#/password/reset/' . $this->data->token
            ]);
    }
}
