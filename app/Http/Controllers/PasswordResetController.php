<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PasswordResetController extends Controller
{
    public function passwordResetRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users'
        ]);

        if ($validator->fails()) {
            return response()->json();
        }

        $token = str_random(32);

        DB::table('password_resets')
            ->updateOrInsert([
                'email' => $request->email
            ], [
                'token' => $token,
                'created_at' => now()
            ]);

        Mail::to($request->email)
            ->send(new ForgotPassword((object) [
                'email' => $request->email,
                'token' => $token
            ]));

        return response()->json();
    }

    public function checkTokenLifeTime(Request $request)
    {
        $current_time = now();

        $valid = DB::table('password_resets')
            ->where('token', $request->token)
            ->where(
                DB::raw("HOUR(TIMEDIFF('${current_time}', created_at))"),
                '<',
                24
            )
            ->exists();

        return response()->json('', $valid ? 200 : 400);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $reset = DB::table('password_resets')
            ->where('token', $request->token)
            ->first(['email']);

        $user = User::query()
            ->where('email', $reset->email)
            ->first();

        $user->password = $request->password;
        $user->save();

        DB::table('password_resets')
            ->where('email', $reset->email)
            ->delete();

        return response()->json();
    }
}
