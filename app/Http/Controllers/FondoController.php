<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Solicitud;
use App\File;
use Illuminate\Support\Facades\Auth;

class FondoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json(Solicitud::query()
                            ->where('user_id', Auth::user()->id)
                            ->where('type_id', '1')
                            ->with('association')
                            ->with('status')
                            ->with('type')->get());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      /*  $fondo = new Request;
        
        $fondo->observation = $request->observation;
        $fondo->amount = $request->amount;
        $fondo->start_date = $request->start_date;
        $fondo->end_date = $request->end_date;
        $fondo->association_id = $request->association_id;
        $fondo->user_id = Auth::user()->id;
        $fondo->type_id = '1';
        $fondo->status_id = '4';
        $fondo->save();*/
        
       Solicitud::create([
            'observation' => $request->observation,
            'association_id' => $request->association_id,
            'user_id' => Auth::user()->id,
            'amount' => $request->amount,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'type_id' => '1',
            'status_id' => '4'
            
        ]);
        
        

        return response()->json([
            'alerta'=>'Fondo Registrado exitosamente' 
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Solicitud::query()->with('association')->with('status')->with('type')->find($id)
            

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $solicitud = Solicitud::query()->with('status')->find($id);
        $solicitud->association_id = $request->association_id;
        $solicitud->observation = $request->observation;
        $solicitud->amount = $request->amount;
        $solicitud->status_id = $request->status_id;
        $solicitud->save();
        return response()->json();
    }

  


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Solicitud::destroy($id);

        return response()->json();
    }
}
