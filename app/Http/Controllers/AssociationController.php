<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Association;

class AssociationController extends Controller
{
    public function active()
    {
        return response()->json(
            Association::query()
                ->with('students.preference.role')
                ->with('students.career')
                ->where('status_id', 1)
                ->get()
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Association::query()
                                ->with('status')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //Association::create($request->all());
        $asociacion = new Association;
        $asociacion->name = $request->name;
        $asociacion->description = $request->description;
        $asociacion->status_id = 4;
        $asociacion->save();

        return response()->json([
            'alerta'=>'Asociacion creada exitosamente'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Association::query()->with('status')
                ->find($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Association::find($id)->update($request->all());

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Association::destroy($id);

        return response()->json();
    }
}
