<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ratificar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class RatificarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getFile($path){
        
        return response()->download(storage_path('app/archivos/'.$path));
    }

    public function index()
    {
        return response()->json(Ratificar::query()
                            ->where('user_id', Auth::user()->id)
                            ->with('association')
                            ->with('status')
                            ->with('type')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $archivo = new Ratificar;
        //$request->file('documento')->store('public');
        //$fileName = time().'.'.$request->documento->getClientOriginalName();
        $path = Storage::putFile('archivos', $request->file('documento'));
        
        $archivo->association_id = $request->association_id;
        $archivo->type_id = '2';
        $archivo->status_id = '3';
        $archivo->path = $path;
        $archivo->user_id = Auth::user()->id;
        $archivo->save();

        return response()->json([
            'alerta'=>'Ratificacion Registrada exitosamente' 
        ]);
        
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Ratificar::query()->with('association')->with('status')->with('type')->with('user')->find($id)
            
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));
        
            $renovacionEdit = Ratificar::find($id); 
            
            $renovacionEdit->path = $path;
            $renovacionEdit->association_id = $request->association_id;
            $renovacionEdit->status_id = $request->status_id;
            $renovacionEdit->type_id = '2';
            $renovacionEdit->user_id = Auth::user()->id;
            $renovacionEdit->save();
            
          

          return response()->json([
               'alerta'=>'Se ha registrado la edición de  la solicitud de renovación'
            ]);

        }else{
            return response()->json([
               'alerta'=>'Debe ajuntar un archivo'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ratificar::destroy($id);

        return response()->json();
    }
}
