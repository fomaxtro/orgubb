<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UniqueCheckerController extends Controller
{
    public function check(Request $request)
    {
        if ($request->has('table')) {
            $query = DB::table($request->table);

            foreach ($request->except('table') as $key => $value) {
                $query->where($key, $value);
            }

            return response()->json([
                'message' => $query->exists()
            ]);
        }

        return response()->json([
            'message' => false
        ]);
    }
}
