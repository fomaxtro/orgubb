<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credential;
use App\Informe;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class InformeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Informe::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required',
            'tipo' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $indexes =  array_map(function ($value) {
            return Credential::query()->firstOrCreate([
                'name' => $value['route'],
                'path' => $value['path']
            ])->id;
        }, $request->credentials);

        Informe::query()
            ->create($request->all());
            /*->credentials()
            ->sync($indexes);*/

        return response()->json('', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Informe::query()
                //->with('preference.role')
                ->find($id)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Informe $informe)
    {
        $validator = Validator::make($request->all(), [
            'descripcion' => 'required',
            'tipo' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $indexes =  array_map(function ($value) {
            return Credential::query()->firstOrCreate(['descripcion' => $value])->id;
        }, $request->credentials);

        $informe->fill($request->all());
        $informe->save();
        //$informe->credentials()->sync($indexes);

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Informe $informe)
    {
        $informe->delete();

        return response()->json();
    }
}
