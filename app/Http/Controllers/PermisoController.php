<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Solicitud;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;

class PermisoController extends Controller
{
    public function getFile($path){
    
        return response()->download(storage_path('app/archivos/'.$path));
    }

    public function informes(Request $request){
        $permisos = $request->data; 
        $pdf = PDF::loadView('pdf.permisos', compact('permisos'));
        $content = $pdf->download()->getOriginalContent();
        Storage::put('archivos/informe.pdf', $content);
        //return PDF::loadFile(public_path().'/archivos/informe.pdf');

        return response()->json(['file' => 'informe.pdf' ]);    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Solicitud::query()
                            ->where('user_id', Auth::user()->id)
                            ->where('type_id', 4)
                            ->with('association')
                            ->with('status')
                            ->with('type')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Solicitud::create([
            'observation' => $request->observation,
            'association_id' => $request->association_id,
            'user_id' => Auth::user()->id,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'type_id' => '4',
            'status_id' => '4'
            
        ]);

        return response()->json([
            'alerta'=>'Permiso Registrado exitosamente' 
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Solicitud::query()->with('association')->with('status')->with('type')->find($id)
            

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $solicitud = Solicitud::query()->with('status')->find($id);
        $solicitud->association_id = $request->association_id;
        $solicitud->observation = $request->observation;
        $solicitud->status_id = $request->status_id;
        $solicitud->save();
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
