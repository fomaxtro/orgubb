<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use App\Utils\Query;
use Fomaxtro\Roles\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return Query::search(User::query()
            ->withTrashed()
            ->with('preference.role'), $request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|regex:/\w[a-z]+( \w[a-z]+)*$/',
            'email' => 'required|email|unique:users|max:100',
            'password' => 'required|confirmed|max:100',
            'role_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        DB::beginTransaction();

        if ($request->student_id != '') {
            $student = Student::query()
                ->findOrFail($request->student_id);

            $data = $request->only('email', 'password');
            $data['name'] = $student->full_name;

            User::query()
                ->create($data)
                ->preferenceCustom()
                ->create($request->only('role_id', 'student_id'));
        } else {
            User::query()
                ->create($request->all())
                ->preference()
                ->create($request->only('role_id'));
        }

        DB::commit();

        return response()->json('', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            User::query()
                ->with('preference.role')
                ->find($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|regex:/\w[a-z]+( \w[a-z]+)*$/',
            'password' => 'confirmed|max:100',
            'role_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        DB::beginTransaction();

        $preference = $user->preferenceCustom;
        $preference->role()->associate(Role::query()->find($request->role_id));

        if ($request->student_id != '') {
            $student = Student::query()
                ->findOrFail($request->student_id);

            $user->name = $student->full_name;
            $preference->student()->associate($student);
        } else {
            $user->name = $request->name;
        }

        if ($request->password != '') {
            $user->password = $request->password;
        }

        $user->save();
        $preference->save();

        DB::commit();

        return response()->json('', 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json();
    }

    public function changePassword(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user->password = $request->password;
        $user->save();

        return response()->json();
    }

    public function restore($id)
    {
        $user = User::query()
            ->withTrashed()
            ->findOrFail($id);

        $user->restore();

        return response()->json([]);
    }
}
