<?php

namespace App\Http\Controllers;

use App\PreferenceCustom;
use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        return response()->json(Student::all());
    }

    public function bind()
    {
        return response()->json(
            Student::query()
                ->join('preferences', 'preferences.student_id', '=', 'students.id')
                ->select('students.*')
                ->whereNull('students.association_id')
                ->get()
        );
    }
}
