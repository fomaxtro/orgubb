<?php

namespace App\Http\Controllers;

use App\Utils\Query;
use Fomaxtro\Roles\Preference;
use Fomaxtro\Roles\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleCustomController extends Controller
{
    public function index(Request $request)
    {
        return Query::search(Role::query(), $request);
    }

    public function transfer(Request $request, Role $role)
    {
        $to_transfer = Role::query()
            ->findOrFail($request->role_id);

        DB::beginTransaction();

        Preference::query()
            ->where('role_id', $role->id)
            ->update([
                'role_id' => $to_transfer->id
            ]);

        $role->delete();

        DB::commit();

        return response()->json([]);
    }
}
