<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ratificar;
use Illuminate\Support\Facades\Auth;

class RatificarAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getFile($path){
        
        return response()->download(storage_path('app/archivos/'.$path));
    }

    public function index()
    {
        return response()->json(Ratificar::query()
                            ->where('user_id', Auth::user()->id)
                            ->with('association')
                            ->with('status')
                            ->with('type')->get());
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(
            Ratificar::query()->with('association')->with('status')->with('type')->with('user')->find($id)
            

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        
            $renovacionEdit = Ratificar::find($id); 

            $renovacionEdit->association_id = $request->association_id;
            $renovacionEdit->status_id = $request->status_id;
            $renovacionEdit->type_id = '2';
            $renovacionEdit->user_id = Auth::user()->id;
            $renovacionEdit->save();
            
            return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ratificar::destroy($id);

        return response()->json();
    }
}
