<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Renewal;


class AdminRenovacionController extends Controller
{
    public function getFile($path){
        
        return response()->download(storage_path('app/archivos/'.$path));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Renewal::query()
                        ->with('association')
                        ->with('status')
                        ->with('type')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //
        if(Renewal::query()->where('association_id',$request->association_id)){
            return response()->json([
               'alerta'=>'La asociación ya tiene registrada una solicitud de renovación'
            ]);

        }
                
        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));

            $renovación = new  Renewal;
            $renovación->path = $path;
            $renovación->association_id = $request->association_id;
            $renovación->status_id = '3';
            $renovación->type_id = '3';
            $renovación->user_id = Auth::user()->id;
            $renovación->save();


            return response()->json([
               'alerta'=>'Se ha registrado la solicitud de renovación'
            ]);

        }else{
            return response()->json([
               'alerta'=>'Debe adjuntar archivo'
            ]);
        }
            
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(Renewal::query()
            ->with('association')
            ->with('user')
            ->with('status')
            ->with('type')
            ->find($id)


        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $renovacionEdit = Renewal::find($id); 
            
            $renovacionEdit->association_id = $request->association_id;
            $renovacionEdit->observation = $request->observation;
            $renovacionEdit->status_id = $request->status_id;
            $renovacionEdit->save();

            return response()->json([
               'alerta'=>'Se ha registrado la edición de  la solicitud de renovación'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
