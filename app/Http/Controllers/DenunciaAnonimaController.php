<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Complaint;


class DenunciaAnonimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('email')){
            
        }
        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));

            $denuncia = new Complaint;
            $denuncia->association_id = $request->association_id;
            $denuncia->observation = $request->observation;
            $denuncia->email = $request->email;
            $denuncia->path = $path;
            $denuncia->user_id = '1055';
            $denuncia->save();

            return response()->json([
               'alerta'=>'Denuncia enviada exitosamente'
            ]);

        }else{  
            $denuncia = new Complaint;
            $denuncia->association_id = $request->association_id;
            $denuncia->observation = $request->observation;
            $denuncia->email = $request->email;
            $denuncia->user_id = '1055';
            $denuncia->save();
        

            return response()->json([
                'alerta'=>'Denuncia enviada exitosamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
