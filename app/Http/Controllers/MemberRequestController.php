<?php

namespace App\Http\Controllers;

use App\Association;
use App\MemberRequest;
use App\Student;
use App\Utils\Query;
use Fomaxtro\Roles\Preference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MemberRequestController extends Controller
{
    public function register(Request $request)
    {
        $student = Student::query()
            ->find($request->student_id);

        $student->association_id = $request->association_id;

        $preference = $student->preferenceCustom;
        $preference->role_id = $request->role_id;

        DB::beginTransaction();

        $student->save();
        $preference->save();

        DB::commit();

        return response()->json([]);
    }

    public function destroy(Student $student)
    {
        $student->association_id = null;

        $preference = $student->preference;
        $preference->role_id = 6;

        DB::beginTransaction();

        $student->save();
        $preference->save();

        DB::commit();

        return response()->json([]);
    }

    public function changeRole(Request $request)
    {
        Preference::query()
            ->findOrFail($request->preference_id)
            ->update([
                'role_id' => $request->role_id
            ]);

        return response()->json([]);
    }

    public function getMembers(Request $request)
    {
        $user = $request->user()->preferenceCustom;

        if (!$user->student) {
            return Query::search(Student::query()
                ->with('career', 'association', 'preferenceCustom')
                ->whereNotNull('association_id'), $request);
        }

        if (!$user->student->association_id) {
            return Query::search(Student::query()
                ->with('career', 'association', 'preferenceCustom')
                ->whereNotNull('association_id'), $request);
        }

        return Query::search(Student::query()
            ->with('career', 'association', 'preferenceCustom')
            ->where('association_id', $user->student->association_id), $request);
    }

    public function process($id, Request $request)
    {
        $mem_request = MemberRequest::query()
            ->findOrFail($id);

        $mem_request->observation_process = $request->observation_process;

        DB::beginTransaction();

        if ($request->resolve) {
            $mem_request->status_id = 1;
            $student = $mem_request->student;
            $student->association_id = $mem_request->association_id;

            $student->save();

            $preference = $student->preferenceCustom;
            $preference->role_id = 5;

            $preference->save();
        } else {
            $mem_request->status_id = 6;
        }

        $mem_request->save();

        DB::commit();

        return response()->json([]);
    }

    public function history(Request $request)
    {
        $user = $request->user()->preferenceCustom;

        $query = MemberRequest::query()
            ->with('student.career', 'association', 'status')
            ->where('status_id', '<>', 4);

        if ($user->student) {
            if ($user->role_id == 6) {
                $query->where('student_id', $user->student_id);
            } else {
                $query->where('association_id', $user->student->association_id);
            }
        }

        return Query::search($query, $request);
    }

    public function index(Request $request)
    {
        $user = $request->user()->preferenceCustom;

        if (!$user->student->association_id) {
            return Query::search(
                MemberRequest::query()
                    ->with('student.career', 'association', 'status')
                    ->where('status_id', 4), $request);
        }

        return Query::search(
            MemberRequest::query()
                ->with('student.career', 'association', 'status')
                ->where('association_id', $user->student->association_id)
                ->where('status_id', 4), $request);
    }

    public function show($id)
    {
        return response()->json(
            MemberRequest::query()
                ->with('student.career')
                ->find($id)
        );
    }

    public function request(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'association_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $student = $request->user()
            ->preferenceCustom
            ->student;

        if ($student->association) {
            return response()->json([
                'message' => 'El estudiante ya pertenece a una asociacion'
            ], 400);
        }

        if ($student->hasRequest()) {
            return response()->json([
                'message' => 'El estudiante posee una solicitud pendiente'
            ], 400);
        }

        DB::beginTransaction();

        MemberRequest::query()
            ->create([
                'student_id' => $student->id,
                'association_id' => $request->association_id,
                'status_id' => 4,
                'observation' => $request->observation,
                'date' => now()->toDateString()
            ]);

        DB::commit();

        return response()->json([], 201);
    }

    public function valid() {
        $student = Auth::user()
            ->preferenceCustom
            ->student;

        if (!$student) {
            return response()->json([
                'message' => 'El usuario no es un estudiante'
            ], 400);
        }

        if ($student->association) {
            return response()->json([
                'message' => 'El estudiante ya pertenece a una asociacion'
            ], 400);
        }

        if ($student->hasRequest()) {
            return response()->json([
                'message' => 'El estudiante posee una solicitud pendiente'
            ], 400);
        }

        return response()->json([]);
    }
}
