<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Renewal;
use Illuminate\Support\Facades\Storage;



class RenovacionController extends Controller
{
    public function getFile($path){
        
        return response()->download(storage_path('app/archivos/'.$path));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Renewal::query()
                        ->where('user_id',Auth::user()->id)
                        ->with('association')
                        ->with('status')
                        ->with('type')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
         if(Renewal::query()->where('association_id',$request->association_id)->exists()){
            return response()->json([
               'result'=>'warning',
               'alerta'=>'La asociación ya tiene registrada una solicitud de renovación'
            ]);

        }

                
        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));

            $renovacion = new  Renewal;
            $renovacion->path = $path;
            $renovacion->association_id = $request->association_id;
            $renovacion->status_id = '3';
            $renovacion->type_id = '3';
            $renovacion->user_id = Auth::user()->id;
            $renovacion->save();


            return response()->json([
               'alerta'=>'Se ha registrado la solicitud de renovación'
            ]);

        }else{
            return response()->json([
               'alerta'=>'Debe ajuntar un archivo'
            ]);
        }
            
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(Renewal::query()
            ->with('association')
            ->with('user')
            ->with('status')
            ->with('type')
            ->find($id)


        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if($request->hasFile('documento')){
        
             $path = Storage::putFile('archivos', $request->file('documento'));
        
            $renovacionEdit = Renewal::find($id);      
            $renovacionEdit->path = $path;
            $renovacionEdit->association_id = $request->association_id;
            $renovacionEdit->status_id = '3';
            $renovacionEdit->type_id = '3';
            $renovacionEdit->user_id = Auth::user()->id;
            $renovacionEdit->save();
            
          

          return response()->json([
               'alerta'=>'Se ha registrado la edición de  la solicitud de renovación'
            ]);

        }else{
            return response()->json([
               'alerta'=>'Debe ajuntar un archivo'
            ]);
        }


    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
