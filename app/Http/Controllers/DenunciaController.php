<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Complaint;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class DenunciaController extends Controller
{
    public function getFile($path){
        

        return response()->download(storage_path('app/archivos/'.$path));
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Complaint::query()->where('user_id',Auth::user()->id)->with('association')->get());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));

            $denuncia = new  Complaint;
            $denuncia->association_id = $request->association_id;
            $denuncia->observation = $request->observation;
            $denuncia->user_id = Auth::user()->id;
            $denuncia->path = $path;
            $denuncia->save();

            return response()->json([
               'alerta'=>'Denuncia enviada exitosamente'
            ]);

        }else{  
            Complaint::create([
                'observation' => $request->observation,
                'association_id' => $request->association_id,
                'user_id' => $request->user()->id
        ]);

            return response()->json([
                'alerta'=>'Denuncia enviada exitosamente'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json(
            Complaint::query()->with('association')->with('user')->find($id)

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        if($request->hasFile('documento')){
        
            $path = Storage::putFile('archivos', $request->file('documento'));

            $denunciaEdit = Complaint::find($id);
            $denunciaEdit->association_id = $request->association_id;
            $denunciaEdit->observation = $request->observation;
            $denunciaEdit->user_id = Auth::user()->id;
            $denunciaEdit->path = $path;
            $denunciaEdit->save(); 

          return response()->json([
               'alerta'=>'Se ha registrado la edición de  la solicitud de renovación'
            ]);

        }else{
            $denunciaEdit = Complaint::find($id);
            $denunciaEdit->association_id = $request->association_id;
            $denunciaEdit->observation = $request->observation;
            $denunciaEdit->user_id = Auth::user()->id;
            $denunciaEdit->save(); 

            return response()->json([
                'alerta'=>'Se ha registrado la edición de  la solicitud de renovación'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Complaint::destroy($id);

        return response()->json();
    }
}
