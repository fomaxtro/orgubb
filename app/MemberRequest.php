<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MemberRequest extends Model
{
    protected $fillable = [
        'student_id', 'association_id', 'status_id', 'observation', 'date'
    ];

    public function getDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function association()
    {
        return $this->belongsTo(Association::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
