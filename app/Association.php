<?php

namespace App;
use App\Status;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{

    protected $fillable = [
        'name', 'description', 'status_id'
    ];

    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
