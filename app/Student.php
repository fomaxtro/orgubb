<?php

namespace App;

use Carbon\Carbon;
use Fomaxtro\Roles\Preference;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'first_name', 'last_name', 'dni', 'dv', 'degree', 'career_id'
    ];

    protected $appends = [
        'full_name', 'full_dni'
    ];

    public function getFullNameAttribute()
    {
        return "{$this->attributes['name']} {$this->attributes['first_name']} {$this->attributes['last_name']}";
    }

    public function getFullDniAttribute()
    {
        return "{$this->attributes['dni']}-{$this->attributes['dv']}";
    }

    public function career()
    {
        return $this->belongsTo(Career::class);
    }

    public function preference()
    {
        return $this->hasOne(Preference::class);
    }

    public function preferenceCustom()
    {
        return $this->hasOne(PreferenceCustom::class);
    }

    public function association()
    {
        return $this->belongsTo(Association::class);
    }

    public function hasRequest()
    {
        return $this->hasMany(MemberRequest::class)
            ->where('status_id', 4)
            ->exists();
    }
}
