<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Informe extends Model
{
    protected $fillable = [
        'descripcion', 'tipo', 'aprobacion'
    ];
}
