<?php

namespace App;
use App\Status;
use App\Association;
use App\User;
use App\Type;

use Illuminate\Database\Eloquent\Model;

class Renewal extends Model
{
    //
    protected $fillable = [
        'path','observation', 'association_id','status_id','user_id','type_id'
    ];


    public function association(){
        return $this->belongsTo(Association::class);
    }

    public function status(){
        return $this->belongsTo(Status::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }
}
