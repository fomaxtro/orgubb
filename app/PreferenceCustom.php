<?php

namespace App;

use Fomaxtro\Roles\Role;
use Illuminate\Database\Eloquent\Model;

class PreferenceCustom extends Model
{
    protected $table = 'preferences';

    protected $fillable = [
        'role_id', 'student_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
