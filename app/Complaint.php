<?php

namespace App;
use App\Association;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    //
    protected $fillable = [
        'observation', 'association_id','user_id','path','email'
    ];

    public function association(){
        return $this->belongsTo(Association::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
