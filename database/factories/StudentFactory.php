<?php

use App\Career;
use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'dni' => $faker->numberBetween(0, 25000000),
        'dv' => $faker->randomDigit,
        'degree' => $faker->company,
        'career_id' => Career::query()
            ->create([
                'code' => $faker->unique()->countryCode,
                'name' => $faker->company
            ])
    ];
});
