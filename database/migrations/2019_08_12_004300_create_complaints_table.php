<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observation');
            $table->string('path')->nullable();
            $table->unsignedInteger('association_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->timestamps();

            $table->foreign('association_id')
                ->references('id')
                ->on('associations');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('complaints');
        Schema::enableForeignKeyConstraints();
    }
}
