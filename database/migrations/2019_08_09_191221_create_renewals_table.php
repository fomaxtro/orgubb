<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRenewalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renewals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('observation')->nullable();
            $table->unsignedInteger('association_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->timestamps();

            $table->foreign('association_id')
                ->references('id')
                ->on('associations');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('type_id')
                ->references('id')
                ->on('types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('renewals');
        Schema::enableForeignKeyConstraints();
    }
}
