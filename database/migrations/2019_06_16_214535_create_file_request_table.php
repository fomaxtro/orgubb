<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_request', function (Blueprint $table) {
            $table->unsignedInteger('request_id');
            $table->unsignedInteger('file_id');

            $table->foreign('request_id')
                ->references('id')
                ->on('requests');

            $table->foreign('file_id')
                ->references('id')
                ->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('file_request');
        Schema::enableForeignKeyConstraints();
    }
}
