<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatificarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratificars', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observation')->nullable();
            $table->text('start_date')->nullable();
            $table->text('end_date')->nullable();
            $table->string('name')->nullable();
            $table->string('path');
            $table->unsignedInteger('association_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('status_id');
            $table->timestamps();

            $table->foreign('association_id')
                ->references('id')
                ->on('associations');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('type_id')
                ->references('id')
                ->on('types');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses');

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('ratificars');
        Schema::enableForeignKeyConstraints();
    }
}
