<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('association_id');
            $table->unsignedInteger('status_id');
            $table->text('observation')->nullable();
            $table->text('observation_process')->nullable();
            $table->date('date');
            $table->timestamps();

            $table->foreign('student_id')
                ->references('id')
                ->on('students');

            $table->foreign('association_id')
                ->references('id')
                ->on('associations');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('member_requests');
        Schema::enableForeignKeyConstraints();
    }
}
