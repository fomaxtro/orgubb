<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()
            ->create([
                'name' => 'Administrator',
               'email' => 'felipe@onesla.com',
                'password' => 'login133'
            ])
            ->preference()
            ->create([
                'role_id' => 1
            ]);

        User::query()
            ->create([
                'name' => 'Sergio Monsalvez',
                'email' => 'sitomusik@hotmail.cl',
                'password' => 'login133'
            ])
            ->preference()
            ->create([
                'role_id' => 1
            ]);

            User::query()
            ->create([
                'id' => '1055',
                'name' => 'Usuario Anonimo',
                'email' => 'anonimo@anonimo.cl',
                'password' => 'login133'
            ])
            ->preference()
            ->create([
                'role_id' => 1
            ]);

            User::query()
            ->create([
                'name' => 'Checho',
                'email' => 'sergio@gmail.com',
                'password' => 'login133'
            ])
            ->preference()
            ->create([
                'role_id' => 1
            ]);
    }
}
