<?php

use App\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::query()
            ->create([
                'name' => 'Activo'
            ]);

        Status::query()
            ->create([
                'name' => 'Inactivo'
            ]);

        Status::query()
            ->create([
                'name' => 'Recibida'
            ]);

        Status::query()
            ->create([
                'name' => 'Pendiente'
            ]);

        Status::query()
            ->create([
                'name' => 'Aprobada'
            ]);

        Status::query()
            ->create([
                'name' => 'Rechazada'
            ]);

        Status::query()
            ->create([
                'name' => 'Falta Documentacion'
            ]);
    }
}
