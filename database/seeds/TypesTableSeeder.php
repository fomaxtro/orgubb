<?php

use App\Type;
use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::query()
            ->create([
                'name' => 'Fondos'
            ]);

        Type::query()
            ->create([
                'name' => 'Ratificacion'
            ]);

        Type::query()
            ->create([
                'name' => 'Renovacion'
            ]);

        Type::query()
            ->create([
                'name' => 'Permisos'
            ]);
    }
}
