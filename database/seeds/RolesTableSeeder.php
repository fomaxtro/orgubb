<?php

use Fomaxtro\Roles\Credential;
use Illuminate\Database\Seeder;
use Fomaxtro\Roles\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()
            ->create([
                'name' => 'Administrador',
                'description' => 'Perfil de control maestro'
            ])
            ->credentials()
            ->attach([
                Credential::query()->firstOrCreate(['name' => '*'])->id
            ]);

        Role::query()
            ->create([
                'name' => 'Presidente',
                'description' => 'Presidente de la asociacion'
            ])
            ->credentials()
            ->attach([
                Credential::query()->firstOrCreate(['name' => 'members.index'])->id,
                Credential::query()->firstOrCreate(['name' => 'pending.index'])->id,
                Credential::query()->firstOrCreate(['name' => 'histories.index'])->id
            ]);

        Role::query()
            ->create([
                'name' => 'Tesorero',
                'description' => 'Tesorero de la asociacion'
            ]);

        Role::query()
            ->create([
                'name' => 'Secretario',
                'description' => 'Secretario de la asociacion'
            ]);

        Role::query()
            ->create([
                'name' => 'Miembro',
                'description' => 'Miembro de la asociacion'
            ])
            ->credentials()
            ->attach([
                Credential::query()->firstOrCreate(['name' => 'members.index'])->id,
                Credential::query()->firstOrCreate(['name' => 'pending.index'])->id,
                Credential::query()->firstOrCreate(['name' => 'histories.index'])->id
            ]);

        Role::query()
            ->create([
                'name' => 'Alumno',
                'description' => 'Perfil base '
            ])
            ->credentials()
            ->attach([
                Credential::query()->firstOrCreate(['name' => 'association.form'])->id,
                Credential::query()->firstOrCreate(['name' => 'pending.index'])->id,
                Credential::query()->firstOrCreate(['name' => 'histories.index'])->id
            ]);
    }
}
