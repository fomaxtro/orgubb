<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', 'AuthenticationController@login');

    Route::middleware('auth:api')->group(function () {
        Route::post('logout', 'AuthenticationController@logout');
        Route::get('user', 'AuthenticationController@user');
    });

    Route::post('password/reset', 'PasswordResetController@passwordResetRequest');
    Route::put('password/change', 'PasswordResetController@changePassword');
    Route::get('password/token', 'PasswordResetController@checkTokenLifetime');
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::apiResource('users', 'UserController');

    Route::put('users/{user}/password', 'UserController@changePassword')
        ->name('users.password');

    Route::post('users/{id}/restore', 'UserController@restore')
        ->name('users.restore');
});

Route::prefix('roles')
    ->name('roles.')
    ->middleware(['auth:api'])
    ->group(function () {
        Route::get('/', 'RoleCustomController@index')->name('index');
        Route::post('/{role}', 'RoleCustomController@transfer')->name('destroy');
    });

Route::middleware('auth:api')->group(function () {
    Route::post('members', 'MemberRequestController@register');
    Route::get('students/bind', 'StudentController@bind');
    Route::get('students', 'StudentController@index');
    Route::get('asociacion/active', 'AssociationController@active');
    Route::post('members/request', 'MemberRequestController@request');
    Route::get('pending', 'MemberRequestController@index');
    Route::get('history', 'MemberRequestController@history');
    Route::get('pending/{id}', 'MemberRequestController@show');
    Route::post('pending/{id}', 'MemberRequestController@process');
    Route::get('members/valid', 'MemberRequestController@valid');
    Route::get('members', 'MemberRequestController@getMembers');
    Route::post('members/role', 'MemberRequestController@changeRole');
    Route::delete('members/{student}', 'MemberRequestController@destroy');
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('asociacion', 'AssociationController')
        ->except(['create', 'edit','index']);
});

Route::get('unique/check', 'UniqueCheckerController@check');
Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('fondo', 'FondoController')
        ->except(['create', 'edit']);
});


Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('ratificar', 'RatificarController')
        ->except(['create', 'edit']);
});


    Route::get('ratificar/archivos/{path}','RatificarController@getFile');

    Route::get('ratificaradmin/archivos/{path}','RatificarAdminController@getFile');


Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('ratificaradmin', 'RatificarAdminController')
        ->except(['create', 'edit']);
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('status', 'StatusController')
        ->except(['create', 'edit']);
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('permiso', 'PermisoController')
        ->except(['create', 'edit']);
});



Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('denuncias', 'DenunciasController')
        ->except(['create', 'edit']);
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('admindenuncias', 'AdminDenunciaController')
        ->except(['create', 'edit']);
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('renovaciones', 'RenovacionController')
        ->except(['create', 'edit']);
});

Route::middleware(['auth:api', 'access'])->group(function () {
    Route::resource('adminrenovaciones', 'AdminRenovacionController')
        ->except(['create', 'edit']);
});

Route::resource('/denuncias', 'DenunciaController')->middleware('auth:api');
Route::resource('/denunciaAnonima', 'DenunciaAnonimaController');
Route::resource('/admindenuncias', 'AdminDenunciaController')->middleware('auth:api');
Route::resource('/renovaciones', 'RenovacionController')->middleware('auth:api');
Route::resource('/adminrenovaciones', 'AdminRenovacionController')->middleware('auth:api');

Route::get('asociacion','AssociationController@index');
Route::get('denuncias/archivos/{path}','DenunciaController@getFile');
Route::get('admindenuncias/archivos/{path}','AdminDenunciaController@getFile');
Route::get('renovaciones/archivos/{path}','RenovacionController@getFile');
Route::get('adminrenovaciones/archivos/{path}','RenovacionController@getFile');
Route::get('permiso/archivos/{path}','PermisoController@getFile');
Route::post('informes/info', 'PermisoController@informes');
Route::get('informes/informePDF', 'PermisoController@informePDF');





