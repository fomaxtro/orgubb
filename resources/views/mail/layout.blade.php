<html>
<body style="font-family: arial, helvetica, sans-serif; color: #424242; margin: 0;">
<!-- <div style="background-color: #24272d; position: fixed; top: 0; left: 0; width: 100%; z-index: 99999; padding-top: 10px; padding-bottom: 10px; padding-left: 10px;"> -->
<header style="border: none; margin: 0; position: relative; display: block; box-sizing: border-box; min-height: 60px; padding-top: 10px; padding-left: 10px;">
    <img src="{{ $message->embed(public_path().'/img/logo.png') }}">
</header>
<div style="margin-left: 10px; margin-right: 10px; text-align: justify;">
    <h1>{{ $title }}</h1>
    <p>{{ $content }}</p>
    <br>
    <p>{{ $sub_content }}</p>
    <hr>
    <p>
        <a href="{{ url($url) }}">{{ url($url) }}</a>
    </p>
    <hr>
    <p>Sistema OrgUBB.</p>
</div>
</body>
</html>