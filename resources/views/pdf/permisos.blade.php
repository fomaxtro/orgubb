@extends('layout')

@section('content')

    <h1>Informe Mensual de Solicitudes de Permisos</h1>
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>Asociación</th>
                <th>Tipo</th>
                <th>Estado</th>
                <th>Observacion</th>
                <th>Fecha de Inicio</th>
                <th>Fecha de Termino</th>
                <th>Fecha de Solicitud</th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($permisos as $permiso)
            <tr>       

                <td>{{ $permiso['association']['name'] }}</td>
                <td>{{ $permiso['type']['name'] }} </td>
                <td>{{ $permiso['status']['name'] }} </td>
                <td>{{ $permiso['observation'] }} </td>
                <td>{{ $permiso['start_date'] }} </td>
                <td>{{ $permiso['end_date'] }} </td>
                <td>{{ $permiso['created_at'] }} </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection