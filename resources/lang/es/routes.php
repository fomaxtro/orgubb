<?php

return [];

return [
    'admin' => [
        'translation' => 'Administrador'
    ],
    'telescope' => [
        'translation' => 'Monitoreo'
    ],
    'credentials' => [
        'translation' => 'Credenciales'
    ],
    'roles' => [
        'translation' => 'Roles',
        'children' => [
            'index' => [
                'translation' => 'Listar Roles'
            ],
            'show' => [
                'translation' => 'Ver Rol'
            ],
            'store' => [
                'translation' => 'Crear Rol'
            ],
            'destroy' => [
                'translation' => 'Eliminar Rol'
            ],
            'update' => [
                'translation' => 'Actualizar Usuario'
            ]
        ]
    ],
    'users' => [
        'translation' => 'Usuarios',
        'children' => [
            'index' => [
                'translation' => 'Listar Usuarios'
            ],
            'show' => [
                'translation' => 'Ver Usuario'
            ],
            'store' => [
                'translation' => 'Crear Usuario'
            ],
            'destroy' => [
                'translation' => 'Eliminar Usuario'
            ],
            'update' => [
                'translation' => 'Actualizar Usuario'
            ]
        ]
    ]
];