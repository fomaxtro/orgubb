export default [
  {
    name: 'Inicio',
    icon: 'ni ni-tv-2 text-primary',
    path: '/dashboard'
  },
  {
    name: 'Miembros',
    icon: 'ni ni-badge',
    path: '/asociaciones/members',
    credential: 'members.index'
  },
  {
    name: 'Postular Asociacion',
    icon: 'ni ni-paper-diploma',
    path: '/requests/form',
    credential: 'association.form'
  },
  {
    name: 'Solicitudes Pendientes',
    icon: 'ni ni-badge',
    path: '/requests/pending',
    credential: 'pending.index'
  },
  {
    name: 'Solicitudes',
    icon: 'ni ni-badge',
    path: '/requests/history',
    credential: 'histories.index'
  },
  {
    name: 'Asociaciones',
    icon: 'ni ni-single-02',
    path: '/asociaciones/index',
    credential: 'asociaciones.index'
  },
  {
    name: 'Admin Asociaciones',
    icon: 'ni ni-single-02',
    path: '/adminasociacion/index',
    credential: 'admasociaciones.index'
  },
  {
    name: 'Permisos',
    icon: 'ni ni-like-2',
    path: '/permiso/index',
    credential: 'permisos.index'
  },
  {
    name: 'Admin Permisos',
    icon: 'ni ni-like-2',
    path: '/adminpermiso/index',
    credential: 'admpermisos.index'
  },
  {
    name: 'Usuarios',
    icon: 'ni ni-circle-08',
    path: '/admin/users',
    credential: 'users.index'
  },
  {
    name: 'Roles',
    icon: 'ni ni-settings-gear-65',
    path: '/admin/roles',
    credential: 'roles.index'
  },
  {
    name: 'Denuncias',
    icon: 'ni ni-archive-2',
    path: '/denuncias/index'
  },
  {
    name: 'Admin Denuncias',
    icon: 'ni ni-archive-2',
    path: '/admindenuncias/index',
    credential: 'admdenuncias.index'
  },
  {
    name: 'Renovaciones',
    icon: 'ni ni-archive-2',
    path: '/renovaciones/index'
  },
  {
    name: 'Admin Renovaciones',
    icon: 'ni ni-archive-2',
    path: '/adminrenovaciones/index'
  },
  {
    name: 'Fondos',
    icon: 'ni ni-money-coins',
    path: '/fondos/index',
    credential: 'fondos.index'
  },
  {
    name: 'Administrar Fondos',
    icon: 'ni ni-money-coins',
    path: '/adminfondos/index',
    credential: 'admfondos.index'
  },
  {
    name: 'Ratificacion',
    icon: 'ni ni-check-bold',
    path: '/ratificar/index',
    credential: 'ratificar.index'
  },
  {
    name: 'Admin Ratificacion',
    icon: 'ni ni-check-bold',
    path: '/adminratificar/index',
    credential: 'admratificar.index'
  }
]
