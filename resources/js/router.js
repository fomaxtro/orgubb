import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
Vue.use(Router)

let members = [
  {
    path: 'members',
    name: 'Miembros',
    component: () => import('./views/Platform/Members/Index.vue')
  },
  {
    path: 'members/create',
    name: 'Nuevo Miembro',
    component: () => import('./views/Platform/Members/Create.vue')
  },
  {
    path: 'members/:id',
    name: 'Miembro',
    component: () => import('./views/Platform/Members/Show.vue')
  }
]

let requests = [
  {
    path: 'form',
    name: 'Postular Asociacion',
    component: () => import('./views/Platform/Requests/Request.vue')
  },
  {
    path: 'pending',
    name: 'Solicitudes Pendientes',
    component: () => import('./views/Platform/Requests/Pending.vue')
  },
  {
    path: 'pending/:id',
    name: 'Solicitud',
    component: () => import('./views/Platform/Requests/Show.vue')
  }
]

let histories = [
  {
    path: 'history',
    name: 'Solicitudes',
    component: () => import('./views/Platform/Histories/Index.vue')
  },
  {
    path: 'history/:id',
    name: 'Solicitud',
    component: () => import('./views/Platform/Histories/Show.vue')
  }
]

let asociaciones = [
  {
    path: 'index',
    name: 'Asociaciones',
    component: () => import('./views/Platform/Asociaciones/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Seccion Registro',
    component: () => import('./views/Platform/Asociaciones/Form.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Asociacion',
    component: () => import('./views/Platform/Asociaciones/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Asociacion',
    component: () => import('./views/Platform/Asociaciones/Edit.vue')
  }
]

let permisos = [
  {
    path: 'index',
    name: 'Permisos',
    component: () => import('./views/Platform/Permiso/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Seccion Registro',
    component: () => import('./views/Platform/Permiso/Form.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Permiso',
    component: () => import('./views/Platform/Permiso/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Permiso',
    component: () => import('./views/Platform/Permiso/Edit.vue')
  }
]

let fondos = [
  {
    path: 'index',
    name: 'Seccion de Fondos',
    component: () => import('./views/Platform/Fondos/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Seccion Registro',
    component: () => import('./views/Platform/Fondos/Form.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Fondo',
    component: () => import('./views/Platform/Fondos/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar fondo ',
    component: () => import('./views/Platform/Fondos/Edit.vue')
  }

]

let ratificaciones = [
  {
    path: 'index',
    name: 'Seccion de Ratificacion',
    component: () => import('./views/Platform/Ratificar/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Seccion Registro',
    component: () => import('./views/Platform/Ratificar/Form.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Ratificacion',
    component: () => import('./views/Platform/Ratificar/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Ratificacion',
    component: () => import('./views/Platform/Ratificar/Edit.vue')
  }
]

let roles = [
  {
    path: 'roles',
    name: 'Roles',
    component: () => import('./views/Platform/Roles/Index.vue'),
    meta: {
      credentials: [
        'roles.index'
      ]
    }
  },
  {
    path: 'roles/create',
    name: 'Registrar Rol',
    component: () => import('./views/Platform/Roles/Form.vue'),
    meta: {
      credentials: [
        'roles.store'
      ]
    }
  },
  {
    path: 'roles/:id',
    name: 'Detalles del Rol',
    component: () => import('./views/Platform/Roles/Show.vue'),
    meta: {
      credentials: [
        'roles.show'
      ]
    }
  },
  {
    path: 'roles/:id/edit',
    name: 'Editar Rol',
    component: () => import('./views/Platform/Roles/Form.vue'),
    meta: {
      credentials: [
        'roles.show',
        'roles.update'
      ]
    }
  }
]

let user = [
  {
    path: 'users',
    name: 'Usuarios',
    component: () => import('./views/Platform/Users/Index.vue')
  },
  {
    path: 'users/create',
    name: 'Registrar Usuario',
    component: () => import('./views/Platform/Users/Form.vue')
  },
  {
    path: 'users/:id',
    name: 'Detalles del Usuario',
    component: () => import('./views/Platform/Users/Show.vue')
  },
  {
    path: 'users/:id/edit',
    name: 'Editar Usuario',
    component: () => import('./views/Platform/Users/Form.vue')
  },
  {
    path:'users/:id/password',
    name: 'Cambiar Contraseña',
    component: () => import('./views/Platform/Users/Password.vue')
  }
]

let denuncias = [
  {
    path: 'index',
    name: 'Sección Denuncias',
    component: () => import('./views/Platform/Denuncia/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Sección de Registro de Denuncias',
    component: () => import('./views/Platform/Denuncia/Form.vue')
  },
  {
    path: 'show/:ide',
    name: 'Ver Denuncia',
    component: () => import('./views/Platform/Denuncia/Show.vue')
  },
  {
    path: 'edit/:ide',
    name: 'Editar Denuncia',
    component: () => import('./views/Platform/Denuncia/Edit.vue')
  }

]

let admindenuncias = [
  {
    path: 'index',
    name: 'Sección Admin Denuncias',
    component: () => import('./views/Platform/AdminDenuncia/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Sección Admin de Registro de Denuncias',
    component: () => import('./views/Platform/AdminDenuncia/Form.vue')
  },
  {
    path: 'show/:id',
    name: 'Sección Admin Ver Denuncia',
    component: () => import('./views/Platform/AdminDenuncia/Show.vue')
  }

]

let renovaciones = [
  {
    path: 'index',
    name: 'Sección Renovaciónes',
    component: () => import('./views/Platform/Renovacion/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Sección de Registro de Renovaciones',
    component: () => import('./views/Platform/Renovacion/Form.vue')
  },
  {
    path: 'show/:idr',
    name: 'Sección Ver Renovacion',
    component: () => import('./views/Platform/Renovacion/Show.vue')
  },
  {
    path: 'edit/:idr',
    name: 'Editar Renovacion',
    component: () => import('./views/Platform/Renovacion/Edit.vue')
  }

]

let adminrenovaciones = [
  {
    path: 'index',
    name: 'Sección Admin Renovaciónes',
    component: () => import('./views/Platform/AdminRenovacion/Index.vue')
  },
  {
    path: 'index/create',
    name: 'Sección Admin de Registro de Renovaciones',
    component: () => import('./views/Platform/AdminRenovacion/Form.vue')
  },
  {
    path: 'show/:idar',
    name: 'Sección Admin Ver Renovaciones',
    component: () => import('./views/Platform/AdminRenovacion/Show.vue')
  },
  {
    path: 'edit/:idar',
    name: 'Sección Admin Editar Renovacion',
    component: () => import('./views/Platform/AdminRenovacion/Edit.vue')
  }

]


let adminfondos = [
  {
    path: 'index',
    name: 'Seccion Administrar Fondos',
    component: () => import('./views/Platform/AdminFondo/Index.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Fondo',
    component: () => import('./views/Platform/AdminFondo/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Fondo',
    component: () => import('./views/Platform/AdminFondo/Edit.vue')
  }
]

let adminratificar = [
  {
    path: 'index',
    name: 'Seccion Administrar Ratificaciones',
    component: () => import('./views/Platform/AdminRatificar/Index.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Ratificacion',
    component: () => import('./views/Platform/AdminRatificar/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Ratificacion',
    component: () => import('./views/Platform/AdminRatificar/Edit.vue')
  }
]

let adminasociaciones = [
  {
    path: 'index',
    name: 'Administrar Asociaciones ',
    component: () => import('./views/Platform/AdminAsociacion/Index.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Asociacion',
    component: () => import('./views/Platform/AdminAsociacion/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Asociacion',
    component: () => import('./views/Platform/AdminAsociacion/Edit.vue')
  }
]

let adminpermisos = [
  {
    path: 'index',
    name: 'Administrar Permisos',
    component: () => import('./views/Platform/AdminPermiso/Index.vue')
  },
  {
    path: 'index/:id',
    name: 'Ver Permiso',
    component: () => import('./views/Platform/AdminPermiso/Show.vue')
  },
  {
    path: 'index/:id/edit',
    name: 'Editar Permiso',
    component: () => import('./views/Platform/AdminPermiso/Edit.vue')
  },
  {
    path:'informes',
    name:'Generar Informe de Actividades',
    component: () => import('./views/Platform/AdminPermiso/Informes.vue')
  }
]


export default new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'dashboard',
      component: DashboardLayout,
      children: [
        {
          path: '/dashboard',
          name: 'Inicio',
          component: () => import('./views/Dashboard.vue')
        }
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/',
      redirect: '/login',
      component: () => import('./layout/AuthLayout.vue'),
      children: [
        {
          path: '/login',
          component: () => import('./views/Login.vue')
        }
      ],
      meta: {
        redirect: true
      }
    },
    {
      path: '/admin',
      redirect: '/admin/users',
      component: DashboardLayout,
      children: [
        ...roles,
        ...user
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/requests',
      redirect: '/requests/pending',
      component: DashboardLayout,
      children: [
        ...requests,
        ...histories
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/denuncias',
      redirect: '/denuncias/index ',
      component: DashboardLayout,
      children: [
        ...denuncias
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/admindenuncias',
      redirect: '/admindenuncias/index',
      component: DashboardLayout,
      children: [
        ...admindenuncias
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/renovaciones',
      redirect: '/renovaciones/index',
      component: DashboardLayout,
      children: [
        ...renovaciones
      ]
    },
    {
      path: '/adminrenovaciones',
      redirect: '/adminrenovaciones/index',
      component: DashboardLayout,
      children: [
        ...adminrenovaciones
      ]
    },
    {
      path: '/asociaciones',
      redirect: '/asociaciones/index',
      component: DashboardLayout,
      children: [
        ...asociaciones,
        ...members
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/fondos',
      redirect: '/fondos/index',
      component: DashboardLayout,
      children: [
        ...fondos
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/ratificar',
      redirect: '/ratificar/index',
      component: DashboardLayout,
      children: [
        ...ratificaciones
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/adminfondos',
      redirect: '/adminfondos/index',
      component: DashboardLayout,
      children: [
        ...adminfondos
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/adminratificar',
      redirect: '/adminratificar/index',
      component: DashboardLayout,
      children: [
        ...adminratificar
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/adminasociacion',
      redirect: '/adminasociacion/index',
      component: DashboardLayout,
      children: [
        ...adminasociaciones
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/adminpermiso',
      redirect: '/adminpermiso/index',
      component: DashboardLayout,
      children: [
        ...adminpermisos
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/permiso',
      redirect: '/permiso/index',
      component: DashboardLayout,
      children: [
        ...permisos
      ],
      meta: {
        auth: true
      }
    },
    {
      path: '/password/reset',
      component: () => import('./views/PasswordResetRequest.vue')
    },
    {
      path: '/password/reset/:token',
      component: () => import('./views/PasswordResetForm.vue')
    },
    {
      path: '/forbidden',
      component: () => import('./views/Forbidden.vue')
    },
    {
      path: '*',
      component: () => import('./views/NotFound.vue')
    },
    {
      path:'/denunciaAnonima',
      component: () => import('./views/DenunciaAnonima.vue')
    }
  ]
})
