import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import ElementUI from 'element-ui'
import VuePromiseBtn from 'vue-promise-btn'
import VeeValidate from 'vee-validate';
import sidebarLinks from '@/sidebarLinks'
import RolesDirectives from '@/plugins/roles-directives'
import {matcher, filterRoutes} from './router-middleware'

// Css
import 'element-ui/lib/theme-chalk/index.css'
import 'vue-promise-btn/dist/vue-promise-btn.css'
import 'sweetalert2/dist/sweetalert2.css'

// Lang
import locale from 'element-ui/lib/locale/lang/es'
import VeeSpanish from 'vee-validate/dist/locale/es';

// Axios
window.axios = require('axios')
window.api = (route) => `${window.location.origin}/api/${route.replace(/^\//, '')}`
window.swal = require('sweetalert2')
window.cookie = require('cookie')

// config
axios.defaults.headers.common['Authorization'] = `Bearer ${cookie.parse(document.cookie).token}`

Vue.config.productionTip = false

Vue.use(ElementUI, { locale })
Vue.use(VuePromiseBtn)

Vue.use(VeeValidate, {
  locale: 'es',
  dictionary: {
    es: VeeSpanish
  }
});

Vue.use(ArgonDashboard)
Vue.use(RolesDirectives)

VeeValidate.Validator.extend('unique', {
    getMessage: field => {
        return `El ${field} ya existe`
    },
    validate: (value, args) => {
        if (args.table !== undefined && args.prop !== undefined) {
            return axios.get(api(`unique/check?table=${args.table}&${args.prop}=${value}`))
                .then(response => {
                    return {
                        valid: !response.data.message
                    }
                })
        }

        return value
    }
})

router.beforeEach(async (to, from, next) => {
  let match = await matcher(to)

  if (!(match instanceof Array)) {
    return next(match)
  }

  document.cookie = cookie.serialize('credentials', JSON.stringify(match))

  instance.$sidebar.sidebarLinks = filterRoutes(match, sidebarLinks)

  next()
})

window.instance = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
