import "@/assets/vendor/nucleo/css/nucleo.css";
import "@/assets/scss/argon.scss";
import globalComponents from "./globalComponents";
import globalDirectives from "./globalDirectives";
import NotificationPlugin from "@/components/NotificationPlugin/index"
import crud from './crud'
import SidebarPlugin from "../components/SidebarPlugin";

export default {
  install(Vue) {
    Vue.use(globalComponents);
    Vue.use(globalDirectives);
    Vue.use(SidebarPlugin, {sidebarLinks: []});
    Vue.use(crud)
    Vue.use(NotificationPlugin);
  }
};
