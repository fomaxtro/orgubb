import CrudList from "../components/Crud/CrudList";
import CrudForm from "../components/Crud/CrudForm";

export default {
  install(Vue) {
    Vue.component(CrudList.name, CrudList)
    Vue.component(CrudForm.name, CrudForm)
  }
}