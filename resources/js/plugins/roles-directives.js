export default {
  install(Vue) {
    Vue.directive('render', {
      bind(el, binding, vnode) {

      },
      inserted(el, binding, vnode) {
        let ref = vnode.context.$children.find(x => x.$el === el)
        let credentials = JSON.parse(cookie.parse(document.cookie).credentials)

        if (binding.arg === 'credentials') {
          if (!(!binding.value.some(x => !credentials.includes(x)) || credentials.includes('*'))) {
            vnode.elm.parentElement.removeChild(vnode.elm)

            if (ref !== undefined) {
              ref.$destroy()
            }
          } else {
            if (ref !== undefined) {
              ref.$emit('post')
            } else {
              el.addEventListener('post', () => vnode.context.$emit('post'))
              el.dispatchEvent(new Event('post'))
            }
          }
        }
      }
    })
  }
}